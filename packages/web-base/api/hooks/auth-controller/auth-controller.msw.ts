/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import {
  faker
} from '@faker-js/faker'
import {
  HttpResponse,
  delay,
  http
} from 'msw'
import type {
  AppResponseAddRemoveUserMultiGroupRes,
  AppResponseAdminUpdatePassRes,
  AppResponseCreateGroupRes,
  AppResponseCreateIdentityRes,
  AppResponseDeleteGroupRes,
  AppResponseDeleteIdentitiesRes,
  AppResponseListGroupRes,
  AppResponseLogDtoRes,
  AppResponseUpdateGroupRes,
  AppResponseUserInfRes,
  AppResponseUserUpdatePassRes
} from '../../models'

export const getUserUpdatePasswordResponseMock = (overrideResponse: any = {}): AppResponseUserUpdatePassRes => ({DATA: faker.helpers.arrayElement([{MESSAGE: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getAdminUpdatePasswordResponseMock = (overrideResponse: any = {}): AppResponseAdminUpdatePassRes => ({DATA: faker.helpers.arrayElement([{MESSAGE: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getCreateIdentitiesResponseMock = (overrideResponse: any = {}): AppResponseCreateIdentityRes => ({DATA: faker.helpers.arrayElement([{IDENTITY: faker.helpers.arrayElement([{ADDITIONAL_PROPERTIES: faker.helpers.arrayElement([{
        [faker.string.alphanumeric(5)]: {}
      }, undefined]), CREATED_AT: faker.helpers.arrayElement([faker.word.sample(), undefined]), CREDENTIALS: faker.helpers.arrayElement([{
        [faker.string.alphanumeric(5)]: {ADDITIONAL_PROPERTIES: faker.helpers.arrayElement([{
        [faker.string.alphanumeric(5)]: {}
      }, undefined]), CONFIG: faker.helpers.arrayElement([{}, undefined]), CREATED_AT: faker.helpers.arrayElement([faker.word.sample(), undefined]), IDENTIFIERS: faker.helpers.arrayElement([Array.from({ length: faker.number.int({ min: 1, max: 10 }) }, (_, i) => i + 1).map(() => (faker.word.sample())), undefined]), TYPE: faker.helpers.arrayElement([faker.helpers.arrayElement(['PASSWORD','OIDC','TOTP','LOOKUP_SECRET','WEBAUTHN','CODE','LINK_RECOVERY','CODE_RECOVERY'] as const), undefined]), UPDATED_AT: faker.helpers.arrayElement([faker.word.sample(), undefined]), VERSION: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), ...overrideResponse}
      }, undefined]), ID: faker.helpers.arrayElement([faker.word.sample(), undefined]), METADATA_ADMIN: faker.helpers.arrayElement([{}, undefined]), METADATA_PUBLIC: faker.helpers.arrayElement([{}, undefined]), ORGANIZATION_ID: faker.helpers.arrayElement([faker.word.sample(), undefined]), RECOVERY_ADDRESSES: faker.helpers.arrayElement([Array.from({ length: faker.number.int({ min: 1, max: 10 }) }, (_, i) => i + 1).map(() => ({ADDITIONAL_PROPERTIES: faker.helpers.arrayElement([{
        [faker.string.alphanumeric(5)]: {}
      }, undefined]), CREATED_AT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ID: faker.helpers.arrayElement([faker.word.sample(), undefined]), UPDATED_AT: faker.helpers.arrayElement([faker.word.sample(), undefined]), VALUE: faker.helpers.arrayElement([faker.word.sample(), undefined]), VIA: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})), undefined]), SCHEMA_ID: faker.helpers.arrayElement([faker.word.sample(), undefined]), SCHEMA_URL: faker.helpers.arrayElement([faker.word.sample(), undefined]), STATE: faker.helpers.arrayElement([faker.helpers.arrayElement(['ACTIVE','INACTIVE'] as const), undefined]), STATE_CHANGED_AT: faker.helpers.arrayElement([faker.word.sample(), undefined]), TRAITS: faker.helpers.arrayElement([{}, undefined]), UPDATED_AT: faker.helpers.arrayElement([faker.word.sample(), undefined]), VERIFIABLE_ADDRESSES: faker.helpers.arrayElement([Array.from({ length: faker.number.int({ min: 1, max: 10 }) }, (_, i) => i + 1).map(() => ({ADDITIONAL_PROPERTIES: faker.helpers.arrayElement([{
        [faker.string.alphanumeric(5)]: {}
      }, undefined]), CREATED_AT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ID: faker.helpers.arrayElement([faker.word.sample(), undefined]), STATUS: faker.helpers.arrayElement([faker.word.sample(), undefined]), UPDATED_AT: faker.helpers.arrayElement([faker.word.sample(), undefined]), VALUE: faker.helpers.arrayElement([faker.word.sample(), undefined]), VERIFIED: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), VERIFIED_AT: faker.helpers.arrayElement([faker.word.sample(), undefined]), VIA: faker.helpers.arrayElement([faker.helpers.arrayElement(['EMAIL','SMS'] as const), undefined]), ...overrideResponse})), undefined]), ...overrideResponse}, undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getDeleteIdentitiesResponseMock = (overrideResponse: any = {}): AppResponseDeleteIdentitiesRes => ({DATA: faker.helpers.arrayElement([{MESSAGE: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getListGroupResponseMock = (overrideResponse: any = {}): AppResponseListGroupRes => ({DATA: faker.helpers.arrayElement([{LIST_GROUP: faker.helpers.arrayElement([Array.from({ length: faker.number.int({ min: 1, max: 10 }) }, (_, i) => i + 1).map(() => ({CREATED_AT: faker.helpers.arrayElement([faker.word.sample(), undefined]), CURRENT_USING_COUNT: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), ID: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), LIMIT_COUNT: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), LIMITED: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), NAME: faker.helpers.arrayElement([faker.word.sample(), undefined]), SITE_CAN_CRAWL: faker.helpers.arrayElement([faker.word.sample(), undefined]), UPDATED_AT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})), undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getUpdateGroupResponseMock = (overrideResponse: any = {}): AppResponseUpdateGroupRes => ({DATA: faker.helpers.arrayElement([{MESSAGE: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getCreateGroupResponseMock = (overrideResponse: any = {}): AppResponseCreateGroupRes => ({DATA: faker.helpers.arrayElement([{USER_GROUP: faker.helpers.arrayElement([{CAN_BRING_COMICO: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KENAZ: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KILEDEL: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CURRENT_USING_COUNT: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), ID: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), LIMIT_COUNT: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), LIMITED: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), NAME: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse}, undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getAddGroupsToUserResponseMock = (overrideResponse: any = {}): AppResponseAddRemoveUserMultiGroupRes => ({DATA: faker.helpers.arrayElement([{EMAIL: faker.helpers.arrayElement([faker.word.sample(), undefined]), GROUPS: faker.helpers.arrayElement([Array.from({ length: faker.number.int({ min: 1, max: 10 }) }, (_, i) => i + 1).map(() => ({CAN_BRING_COMICO: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KENAZ: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KILEDEL: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CURRENT_USING_COUNT: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), ID: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), LIMIT_COUNT: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), LIMITED: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), NAME: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})), undefined]), ID: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), ROLE: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getSaveLogoutLogResponseMock = (overrideResponse: any = {}): AppResponseLogDtoRes => ({DATA: faker.helpers.arrayElement([{MESSAGE: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getSaveLoginFailedLogResponseMock = (overrideResponse: any = {}): AppResponseLogDtoRes => ({DATA: faker.helpers.arrayElement([{MESSAGE: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getGetUserInfoResponseMock = (overrideResponse: any = {}): AppResponseUserInfRes => ({DATA: faker.helpers.arrayElement([{EMAIL: faker.helpers.arrayElement([faker.word.sample(), undefined]), GROUPS: faker.helpers.arrayElement([Array.from({ length: faker.number.int({ min: 1, max: 10 }) }, (_, i) => i + 1).map(() => ({CAN_BRING_COMICO: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KENAZ: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KILEDEL: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CURRENT_USING_COUNT: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), ID: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), LIMIT_COUNT: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), LIMITED: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), NAME: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})), undefined]), ID: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), PERMISSION: faker.helpers.arrayElement([{CAN_AUTO_TRANSLATE_PS_EXT: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_COMICO: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KENAZ: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KILEDEL: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_CRAWL: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_DELETE_CHAPTER: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_DELETE_COMIC: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_IMPORT_TRANSLATE_PS_EXT: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_OCR: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_TRANSLATE: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_UPDATE_CHAPTER_NAME: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_UPDATE_COMIC_NAME: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_UPLOAD: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), ...overrideResponse}, undefined]), PS_FLAG: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), RESIZED_WIDTH: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), ROLE: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getGetUserInfoByAdminResponseMock = (overrideResponse: any = {}): AppResponseUserInfRes => ({DATA: faker.helpers.arrayElement([{EMAIL: faker.helpers.arrayElement([faker.word.sample(), undefined]), GROUPS: faker.helpers.arrayElement([Array.from({ length: faker.number.int({ min: 1, max: 10 }) }, (_, i) => i + 1).map(() => ({CAN_BRING_COMICO: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KENAZ: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KILEDEL: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CURRENT_USING_COUNT: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), ID: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), LIMIT_COUNT: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), LIMITED: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), NAME: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})), undefined]), ID: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), PERMISSION: faker.helpers.arrayElement([{CAN_AUTO_TRANSLATE_PS_EXT: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_COMICO: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KENAZ: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KILEDEL: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_CRAWL: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_DELETE_CHAPTER: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_DELETE_COMIC: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_IMPORT_TRANSLATE_PS_EXT: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_OCR: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_TRANSLATE: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_UPDATE_CHAPTER_NAME: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_UPDATE_COMIC_NAME: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_UPLOAD: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), ...overrideResponse}, undefined]), PS_FLAG: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), RESIZED_WIDTH: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), ROLE: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getRemoveGroupsFromUserResponseMock = (overrideResponse: any = {}): AppResponseAddRemoveUserMultiGroupRes => ({DATA: faker.helpers.arrayElement([{EMAIL: faker.helpers.arrayElement([faker.word.sample(), undefined]), GROUPS: faker.helpers.arrayElement([Array.from({ length: faker.number.int({ min: 1, max: 10 }) }, (_, i) => i + 1).map(() => ({CAN_BRING_COMICO: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KENAZ: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CAN_BRING_KILEDEL: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), CURRENT_USING_COUNT: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), ID: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), LIMIT_COUNT: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), LIMITED: faker.helpers.arrayElement([faker.datatype.boolean(), undefined]), NAME: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})), undefined]), ID: faker.helpers.arrayElement([faker.number.int({min: undefined, max: undefined}), undefined]), ROLE: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})

export const getDeleteGroupResponseMock = (overrideResponse: any = {}): AppResponseDeleteGroupRes => ({DATA: faker.helpers.arrayElement([{MESSAGE: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse}, undefined]), ERR_CODE: faker.helpers.arrayElement([faker.word.sample(), undefined]), REASON: faker.helpers.arrayElement([faker.word.sample(), undefined]), RESULT: faker.helpers.arrayElement([faker.word.sample(), undefined]), ...overrideResponse})


export const getUserUpdatePasswordMockHandler = (overrideResponse?: AppResponseUserUpdatePassRes) => {
  return http.put('*/api-v1/auth/identities/update-password', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getUserUpdatePasswordResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getAdminUpdatePasswordMockHandler = (overrideResponse?: AppResponseAdminUpdatePassRes) => {
  return http.put('*/api-v1/auth/admin/identities', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getAdminUpdatePasswordResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getCreateIdentitiesMockHandler = (overrideResponse?: AppResponseCreateIdentityRes) => {
  return http.post('*/api-v1/auth/admin/identities', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getCreateIdentitiesResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getDeleteIdentitiesMockHandler = (overrideResponse?: AppResponseDeleteIdentitiesRes) => {
  return http.delete('*/api-v1/auth/admin/identities', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getDeleteIdentitiesResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getListGroupMockHandler = (overrideResponse?: AppResponseListGroupRes) => {
  return http.get('*/api-v1/auth/admin/group', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getListGroupResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getUpdateGroupMockHandler = (overrideResponse?: AppResponseUpdateGroupRes) => {
  return http.put('*/api-v1/auth/admin/group', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getUpdateGroupResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getCreateGroupMockHandler = (overrideResponse?: AppResponseCreateGroupRes) => {
  return http.post('*/api-v1/auth/admin/group', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getCreateGroupResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getAddGroupsToUserMockHandler = (overrideResponse?: AppResponseAddRemoveUserMultiGroupRes) => {
  return http.put('*/api-v1/auth/admin/add-groups-to-user', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getAddGroupsToUserResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getSaveLogoutLogMockHandler = (overrideResponse?: AppResponseLogDtoRes) => {
  return http.post('*/api-v1/auth/logout', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getSaveLogoutLogResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getSaveLoginFailedLogMockHandler = (overrideResponse?: AppResponseLogDtoRes) => {
  return http.post('*/api-v1/auth/login-failed', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getSaveLoginFailedLogResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getGetUserInfoMockHandler = (overrideResponse?: AppResponseUserInfRes) => {
  return http.get('*/api-v1/auth/info', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getGetUserInfoResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getGetUserInfoByAdminMockHandler = (overrideResponse?: AppResponseUserInfRes) => {
  return http.get('*/api-v1/auth/admin/info', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getGetUserInfoByAdminResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getRemoveGroupsFromUserMockHandler = (overrideResponse?: AppResponseAddRemoveUserMultiGroupRes) => {
  return http.delete('*/api-v1/auth/admin/remove-groups-from-user', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getRemoveGroupsFromUserResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}

export const getDeleteGroupMockHandler = (overrideResponse?: AppResponseDeleteGroupRes) => {
  return http.delete('*/api-v1/auth/admin/delete-group', async () => {
    await delay(1000);
    return new HttpResponse(JSON.stringify(overrideResponse !== undefined ? overrideResponse : getDeleteGroupResponseMock()),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
  })
}
export const getAuthControllerMock = () => [
  getUserUpdatePasswordMockHandler(),
  getAdminUpdatePasswordMockHandler(),
  getCreateIdentitiesMockHandler(),
  getDeleteIdentitiesMockHandler(),
  getListGroupMockHandler(),
  getUpdateGroupMockHandler(),
  getCreateGroupMockHandler(),
  getAddGroupsToUserMockHandler(),
  getSaveLogoutLogMockHandler(),
  getSaveLoginFailedLogMockHandler(),
  getGetUserInfoMockHandler(),
  getGetUserInfoByAdminMockHandler(),
  getRemoveGroupsFromUserMockHandler(),
  getDeleteGroupMockHandler()
]
