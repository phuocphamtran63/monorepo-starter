/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import {
  useMutation
} from '@tanstack/vue-query'
import type {
  MutationFunction,
  UseMutationOptions,
  UseMutationReturnType
} from '@tanstack/vue-query'
import {
  unref
} from 'vue'
import type {
  MaybeRef
} from 'vue'
import type {
  AppResponseGetChapterDetailRes,
  AppResponseObject,
  CreateUpdateChapterDto,
  DeleteChaptersParams,
  GetChapterDetailReq,
  LogReq,
  RenameReq,
  SearchReqGetListChapterReq
} from '../../models'
import { customInstance } from '../../mutator/custom-instance';
import type { ErrorType } from '../../mutator/custom-instance';

type AwaitedInput<T> = PromiseLike<T> | T;

      type Awaited<O> = O extends AwaitedInput<infer T> ? T : never;



export const renameComicOrChapter = (
    renameReq: MaybeRef<RenameReq>,
 ) => {
      renameReq = unref(renameReq);
      
      return customInstance<AppResponseObject>(
      {url: `/api/work/RENAME_COMIC_OR_CHAPTER`, method: 'POST',
      headers: {'Content-Type': 'application/json', },
      data: renameReq
    },
      );
    }
  


export const getRenameComicOrChapterMutationOptions = <TError = ErrorType<unknown>,
    TContext = unknown>(options?: { mutation?:UseMutationOptions<Awaited<ReturnType<typeof renameComicOrChapter>>, TError,{data: RenameReq}, TContext>, }
): UseMutationOptions<Awaited<ReturnType<typeof renameComicOrChapter>>, TError,{data: RenameReq}, TContext> => {
const {mutation: mutationOptions} = options ?? {};

      


      const mutationFn: MutationFunction<Awaited<ReturnType<typeof renameComicOrChapter>>, {data: RenameReq}> = (props) => {
          const {data} = props ?? {};

          return  renameComicOrChapter(data,)
        }

        


  return  { mutationFn, ...mutationOptions }}

    export type RenameComicOrChapterMutationResult = NonNullable<Awaited<ReturnType<typeof renameComicOrChapter>>>
    export type RenameComicOrChapterMutationBody = RenameReq
    export type RenameComicOrChapterMutationError = ErrorType<unknown>

    export const useRenameComicOrChapter = <TError = ErrorType<unknown>,
    TContext = unknown>(options?: { mutation?:UseMutationOptions<Awaited<ReturnType<typeof renameComicOrChapter>>, TError,{data: RenameReq}, TContext>, }
): UseMutationReturnType<
        Awaited<ReturnType<typeof renameComicOrChapter>>,
        TError,
        {data: RenameReq},
        TContext
      > => {

      const mutationOptions = getRenameComicOrChapterMutationOptions(options);

      return useMutation(mutationOptions);
    }
    export const getListChapter = (
    searchReqGetListChapterReq: MaybeRef<SearchReqGetListChapterReq>,
 ) => {
      searchReqGetListChapterReq = unref(searchReqGetListChapterReq);
      
      return customInstance<AppResponseObject>(
      {url: `/api/work/GET_LIST_CHAPTER`, method: 'POST',
      headers: {'Content-Type': 'application/json', },
      data: searchReqGetListChapterReq
    },
      );
    }
  


export const getGetListChapterMutationOptions = <TError = ErrorType<unknown>,
    TContext = unknown>(options?: { mutation?:UseMutationOptions<Awaited<ReturnType<typeof getListChapter>>, TError,{data: SearchReqGetListChapterReq}, TContext>, }
): UseMutationOptions<Awaited<ReturnType<typeof getListChapter>>, TError,{data: SearchReqGetListChapterReq}, TContext> => {
const {mutation: mutationOptions} = options ?? {};

      


      const mutationFn: MutationFunction<Awaited<ReturnType<typeof getListChapter>>, {data: SearchReqGetListChapterReq}> = (props) => {
          const {data} = props ?? {};

          return  getListChapter(data,)
        }

        


  return  { mutationFn, ...mutationOptions }}

    export type GetListChapterMutationResult = NonNullable<Awaited<ReturnType<typeof getListChapter>>>
    export type GetListChapterMutationBody = SearchReqGetListChapterReq
    export type GetListChapterMutationError = ErrorType<unknown>

    export const useGetListChapter = <TError = ErrorType<unknown>,
    TContext = unknown>(options?: { mutation?:UseMutationOptions<Awaited<ReturnType<typeof getListChapter>>, TError,{data: SearchReqGetListChapterReq}, TContext>, }
): UseMutationReturnType<
        Awaited<ReturnType<typeof getListChapter>>,
        TError,
        {data: SearchReqGetListChapterReq},
        TContext
      > => {

      const mutationOptions = getGetListChapterMutationOptions(options);

      return useMutation(mutationOptions);
    }
    export const getChapterDetail = (
    getChapterDetailReq: MaybeRef<GetChapterDetailReq>,
 ) => {
      getChapterDetailReq = unref(getChapterDetailReq);
      
      return customInstance<AppResponseGetChapterDetailRes>(
      {url: `/api/work/GET_CHAPTER_DETAIL`, method: 'POST',
      headers: {'Content-Type': 'application/json', },
      data: getChapterDetailReq
    },
      );
    }
  


export const getGetChapterDetailMutationOptions = <TError = ErrorType<unknown>,
    TContext = unknown>(options?: { mutation?:UseMutationOptions<Awaited<ReturnType<typeof getChapterDetail>>, TError,{data: GetChapterDetailReq}, TContext>, }
): UseMutationOptions<Awaited<ReturnType<typeof getChapterDetail>>, TError,{data: GetChapterDetailReq}, TContext> => {
const {mutation: mutationOptions} = options ?? {};

      


      const mutationFn: MutationFunction<Awaited<ReturnType<typeof getChapterDetail>>, {data: GetChapterDetailReq}> = (props) => {
          const {data} = props ?? {};

          return  getChapterDetail(data,)
        }

        


  return  { mutationFn, ...mutationOptions }}

    export type GetChapterDetailMutationResult = NonNullable<Awaited<ReturnType<typeof getChapterDetail>>>
    export type GetChapterDetailMutationBody = GetChapterDetailReq
    export type GetChapterDetailMutationError = ErrorType<unknown>

    export const useGetChapterDetail = <TError = ErrorType<unknown>,
    TContext = unknown>(options?: { mutation?:UseMutationOptions<Awaited<ReturnType<typeof getChapterDetail>>, TError,{data: GetChapterDetailReq}, TContext>, }
): UseMutationReturnType<
        Awaited<ReturnType<typeof getChapterDetail>>,
        TError,
        {data: GetChapterDetailReq},
        TContext
      > => {

      const mutationOptions = getGetChapterDetailMutationOptions(options);

      return useMutation(mutationOptions);
    }
    export const createUpdateChapter = (
    createUpdateChapterDto: MaybeRef<CreateUpdateChapterDto>,
 ) => {
      createUpdateChapterDto = unref(createUpdateChapterDto);
      
      return customInstance<AppResponseObject>(
      {url: `/api/work/CREATE_UPDATE_CHAPTER`, method: 'POST',
      headers: {'Content-Type': 'application/json', },
      data: createUpdateChapterDto
    },
      );
    }
  


export const getCreateUpdateChapterMutationOptions = <TError = ErrorType<unknown>,
    TContext = unknown>(options?: { mutation?:UseMutationOptions<Awaited<ReturnType<typeof createUpdateChapter>>, TError,{data: CreateUpdateChapterDto}, TContext>, }
): UseMutationOptions<Awaited<ReturnType<typeof createUpdateChapter>>, TError,{data: CreateUpdateChapterDto}, TContext> => {
const {mutation: mutationOptions} = options ?? {};

      


      const mutationFn: MutationFunction<Awaited<ReturnType<typeof createUpdateChapter>>, {data: CreateUpdateChapterDto}> = (props) => {
          const {data} = props ?? {};

          return  createUpdateChapter(data,)
        }

        


  return  { mutationFn, ...mutationOptions }}

    export type CreateUpdateChapterMutationResult = NonNullable<Awaited<ReturnType<typeof createUpdateChapter>>>
    export type CreateUpdateChapterMutationBody = CreateUpdateChapterDto
    export type CreateUpdateChapterMutationError = ErrorType<unknown>

    export const useCreateUpdateChapter = <TError = ErrorType<unknown>,
    TContext = unknown>(options?: { mutation?:UseMutationOptions<Awaited<ReturnType<typeof createUpdateChapter>>, TError,{data: CreateUpdateChapterDto}, TContext>, }
): UseMutationReturnType<
        Awaited<ReturnType<typeof createUpdateChapter>>,
        TError,
        {data: CreateUpdateChapterDto},
        TContext
      > => {

      const mutationOptions = getCreateUpdateChapterMutationOptions(options);

      return useMutation(mutationOptions);
    }
    export const createManualLog = (
    logReq: MaybeRef<LogReq>,
 ) => {
      logReq = unref(logReq);
      
      return customInstance<void>(
      {url: `/api/work/CREATE_MANUAL_LOG`, method: 'POST',
      headers: {'Content-Type': 'application/json', },
      data: logReq
    },
      );
    }
  


export const getCreateManualLogMutationOptions = <TError = ErrorType<unknown>,
    TContext = unknown>(options?: { mutation?:UseMutationOptions<Awaited<ReturnType<typeof createManualLog>>, TError,{data: LogReq}, TContext>, }
): UseMutationOptions<Awaited<ReturnType<typeof createManualLog>>, TError,{data: LogReq}, TContext> => {
const {mutation: mutationOptions} = options ?? {};

      


      const mutationFn: MutationFunction<Awaited<ReturnType<typeof createManualLog>>, {data: LogReq}> = (props) => {
          const {data} = props ?? {};

          return  createManualLog(data,)
        }

        


  return  { mutationFn, ...mutationOptions }}

    export type CreateManualLogMutationResult = NonNullable<Awaited<ReturnType<typeof createManualLog>>>
    export type CreateManualLogMutationBody = LogReq
    export type CreateManualLogMutationError = ErrorType<unknown>

    export const useCreateManualLog = <TError = ErrorType<unknown>,
    TContext = unknown>(options?: { mutation?:UseMutationOptions<Awaited<ReturnType<typeof createManualLog>>, TError,{data: LogReq}, TContext>, }
): UseMutationReturnType<
        Awaited<ReturnType<typeof createManualLog>>,
        TError,
        {data: LogReq},
        TContext
      > => {

      const mutationOptions = getCreateManualLogMutationOptions(options);

      return useMutation(mutationOptions);
    }
    export const deleteChapters = (
    params: MaybeRef<DeleteChaptersParams>,
 ) => {
      params = unref(params);
      
      return customInstance<AppResponseObject>(
      {url: `/api/work/DELETE_LIST_CHAPTER`, method: 'DELETE',
        params: unref(params)
    },
      );
    }
  


export const getDeleteChaptersMutationOptions = <TError = ErrorType<unknown>,
    TContext = unknown>(options?: { mutation?:UseMutationOptions<Awaited<ReturnType<typeof deleteChapters>>, TError,{params: DeleteChaptersParams}, TContext>, }
): UseMutationOptions<Awaited<ReturnType<typeof deleteChapters>>, TError,{params: DeleteChaptersParams}, TContext> => {
const {mutation: mutationOptions} = options ?? {};

      


      const mutationFn: MutationFunction<Awaited<ReturnType<typeof deleteChapters>>, {params: DeleteChaptersParams}> = (props) => {
          const {params} = props ?? {};

          return  deleteChapters(params,)
        }

        


  return  { mutationFn, ...mutationOptions }}

    export type DeleteChaptersMutationResult = NonNullable<Awaited<ReturnType<typeof deleteChapters>>>
    
    export type DeleteChaptersMutationError = ErrorType<unknown>

    export const useDeleteChapters = <TError = ErrorType<unknown>,
    TContext = unknown>(options?: { mutation?:UseMutationOptions<Awaited<ReturnType<typeof deleteChapters>>, TError,{params: DeleteChaptersParams}, TContext>, }
): UseMutationReturnType<
        Awaited<ReturnType<typeof deleteChapters>>,
        TError,
        {params: DeleteChaptersParams},
        TContext
      > => {

      const mutationOptions = getDeleteChaptersMutationOptions(options);

      return useMutation(mutationOptions);
    }
    