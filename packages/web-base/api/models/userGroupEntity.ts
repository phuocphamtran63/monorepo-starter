/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */

export interface UserGroupEntity {
  CREATED_AT?: string;
  CURRENT_USING_COUNT?: number;
  ID?: number;
  LIMIT_COUNT?: number;
  LIMITED?: boolean;
  NAME?: string;
  SITE_CAN_CRAWL?: string;
  UPDATED_AT?: string;
}
