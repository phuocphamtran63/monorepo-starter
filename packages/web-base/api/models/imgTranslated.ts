/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import type { ImgTranslatedJSON } from './imgTranslatedJSON';

export interface ImgTranslated {
  CREATED_AT?: string;
  IMG_ID?: number;
  IMG_NO?: number;
  JSON?: ImgTranslatedJSON;
  UPDATED_AT?: string;
  URL?: string;
}
