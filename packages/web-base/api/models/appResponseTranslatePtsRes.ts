/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import type { TranslatePtsRes } from './translatePtsRes';

export interface AppResponseTranslatePtsRes {
  DATA?: TranslatePtsRes;
  ERR_CODE?: string;
  REASON?: string;
  RESULT?: string;
}
