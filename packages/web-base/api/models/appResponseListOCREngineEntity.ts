/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import type { OCREngineEntity } from './oCREngineEntity';

export interface AppResponseListOCREngineEntity {
  DATA?: OCREngineEntity[];
  ERR_CODE?: string;
  REASON?: string;
  RESULT?: string;
}
