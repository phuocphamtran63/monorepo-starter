/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import type { ChapterEntity } from './chapterEntity';
import type { ImageEntityJSON } from './imageEntityJSON';
import type { LanguageEntity } from './languageEntity';

export interface ImageEntity {
  BUCKET_NAME?: string;
  CHAPTER?: ChapterEntity;
  CREATED_AT?: string;
  DISABLED?: boolean;
  HEIGHT?: number;
  ID?: number;
  IMAGE_NAME?: string;
  IMAGE_ORDER?: number;
  IMG_NO?: number;
  IS_MIGRATE_IMAGE?: boolean;
  JSON?: ImageEntityJSON;
  LANGUAGE?: LanguageEntity;
  PREVENT_UPDATED_AT?: boolean;
  REGION?: string;
  RESIZED_URL?: string;
  UPDATED_AT?: string;
  URL?: string;
  WIDTH?: number;
}
