/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */

export interface ComicPermissionRes {
  CAN_BRING?: boolean;
  CAN_CRAWL?: boolean;
  CAN_UPLOAD?: boolean;
}
