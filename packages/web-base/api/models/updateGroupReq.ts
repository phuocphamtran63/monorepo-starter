/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */

export interface UpdateGroupReq {
  CAN_BRING_COMICO?: boolean;
  CAN_BRING_KENAZ?: boolean;
  CAN_BRING_KILEDEL?: boolean;
  /** @minimum 1 */
  GROUP_ID: number;
  /** @minimum 1 */
  LIMIT_COUNT?: number;
  LIMITED?: boolean;
}
