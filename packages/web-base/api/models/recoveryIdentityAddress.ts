/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import type { RecoveryIdentityAddressADDITIONALPROPERTIES } from './recoveryIdentityAddressADDITIONALPROPERTIES';

export interface RecoveryIdentityAddress {
  ADDITIONAL_PROPERTIES?: RecoveryIdentityAddressADDITIONALPROPERTIES;
  CREATED_AT?: string;
  ID?: string;
  UPDATED_AT?: string;
  VALUE?: string;
  VIA?: string;
}
