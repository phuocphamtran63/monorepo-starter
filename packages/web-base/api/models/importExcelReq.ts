/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import type { NoteTextBoxDTO } from './noteTextBoxDTO';
import type { TranslatedTextBoxDTO } from './translatedTextBoxDTO';

export interface ImportExcelReq {
  DELETE_TEXT_BOX_IDS?: number[];
  NOTE_TEXT_BOXES?: NoteTextBoxDTO[];
  TRANSLATED_TEXT_BOXES?: TranslatedTextBoxDTO[];
}
