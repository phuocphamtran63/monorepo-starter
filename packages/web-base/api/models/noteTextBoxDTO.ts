/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */

export interface NoteTextBoxDTO {
  BACKGROUND_COLOR?: string;
  CAN_EDIT_BACKGROUND?: boolean;
  COMBINED_X?: number;
  COMBINED_Y?: number;
  FEEDBACK?: boolean;
  FONT_FAMILY?: string;
  FONT_SIZE?: string;
  HEIGHT?: number;
  ID?: number;
  MANUAL_CREATE_TEXT_BOX?: boolean;
  NOTE_TEXT?: string;
  ROTATE_DEGREE?: number;
  TEXT_ALIGN?: string;
  TEXT_COLOR?: string;
  WIDTH?: number;
}
