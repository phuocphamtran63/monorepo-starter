/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import type { ComicEntity } from './comicEntity';
import type { ChapterEntityCRAWLERDATA } from './chapterEntityCRAWLERDATA';
import type { ImageEntity } from './imageEntity';
import type { LanguageEntity } from './languageEntity';

export interface ChapterEntity {
  BRING_CHAPTER?: boolean;
  CHAPTER_NAME?: string;
  CHAPTER_SITE_ID?: string;
  COMIC?: ComicEntity;
  CRAWLER_DATA?: ChapterEntityCRAWLERDATA;
  CREATED_AT?: string;
  DEFAULT_TRANSLATED_LOCALE_CODE?: string;
  DRAFT?: boolean;
  FORCE_RESIZE_IMAGE?: boolean;
  FROM_SITE?: string;
  GETTING_TYPE_NAME?: string;
  ID?: number;
  IMAGES?: ImageEntity[];
  IMPORTED_EXCEL_COUNT?: number;
  IS_MIGRATE?: boolean;
  IS_VISION_FINISHED?: boolean;
  LANGUAGES?: LanguageEntity[];
  NEW_API?: boolean;
  OCR_PERMISSION?: boolean;
  ORIGINAL_LOCALE_CODE?: string;
  OWNER?: string;
  STATUS?: string;
  TRANSLATE_PERMISSION?: boolean;
  TRANSLATED?: boolean;
  TRANSLATED_COUNT?: number;
  UPDATED_AT?: string;
  UPLOAD?: boolean;
  VISION_END_AT?: string;
  VISION_START_AT?: string;
  ZIP_URL?: string;
}
