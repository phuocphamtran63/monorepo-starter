/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import type { GetListChapterReq } from './getListChapterReq';

export interface SearchReqGetListChapterReq {
  CONDITIONS: GetListChapterReq;
  SEARCH_PAGE?: number;
  SEARCH_UNIT?: number;
}
