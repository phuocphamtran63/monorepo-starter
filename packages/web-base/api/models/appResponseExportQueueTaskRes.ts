/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import type { ExportQueueTaskRes } from './exportQueueTaskRes';

export interface AppResponseExportQueueTaskRes {
  DATA?: ExportQueueTaskRes;
  ERR_CODE?: string;
  REASON?: string;
  RESULT?: string;
}
