/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */

export interface UserGroup {
  CAN_BRING_COMICO?: boolean;
  CAN_BRING_KENAZ?: boolean;
  CAN_BRING_KILEDEL?: boolean;
  CURRENT_USING_COUNT?: number;
  ID?: number;
  LIMIT_COUNT?: number;
  LIMITED?: boolean;
  NAME?: string;
}
