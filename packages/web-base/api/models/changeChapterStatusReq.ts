/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import type { ChangeChapterStatusReqSTATUS } from './changeChapterStatusReqSTATUS';

export interface ChangeChapterStatusReq {
  STATUS?: ChangeChapterStatusReqSTATUS;
}
