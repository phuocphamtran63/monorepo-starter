/**
 * Generated by orval v6.28.2 🍺
 * Do not edit manually.
 * OpenAPI definition
 * OpenAPI spec version: v0
 */
import type { ImageJSON } from './imageJSON';

export interface Image {
  ID?: number;
  IMG_NO?: number;
  JSON?: ImageJSON;
  OBJECT_KEY?: string;
}
