# Welcome to web-base 👋

## Dependencies
- Add --dev flag when installing devDependencies if only needed for development and testing.
- Add # why you need this package
```shell
yarn add sass nuxt-headlessui #config style
yarn add @pinia/nuxt #config flexible Store
yarn add @nuxtjs/i18n #config specific language
yarn add @vueuse/core @vueuse/components @vueuse/nuxt @vueuse/router #config collection of utility functions
yarn add @nuxt/test-utils vitest @vue/test-utils happy-dom playwright-core #config unit test run on a Nuxt runtime environment.
yarn add axios @tanstack/vue-query  #config fetch data
yarn add sweetalert2 #config alert
yarn add @vee-validate/nuxt @vee-validate/zod zod nuxt-zod-i18n #form validation
yarn add @nuxt/ui #config UI components
yarn add maska #config format input
```
## Rule
> Pinia Store Naming and Usage Rule

- Store files are named `use...Store.ts`, where `...` is replaced with the specific name of the store. For example, a store for managing user data might be named `useUserStore.ts`.

- Inside these store files, we define a `use...Store()` function that sets up and returns the store. This function is then called inside a `setup()` function to initialize the store.

## Usage
> Localization
- [Nuxt i18n](https://i18n.nuxtjs.org/docs/guide): **Note:  Routing Strategies is important**

> Pinia
- Defining a Store: [Setup Stores](https://pinia.vuejs.org/core-concepts/)

> Vee-validate & Zod (Form Validation)
- Zod Schema Validation https://vee-validate.logaretm.com/v4/integrations/zod-schema-validation/
- Zod localization:
  + Zod Issue code: https://zod.dev/ERROR_HANDLING?id=zodissuecode

> Nuxt UI
- Nuxt Icon: https://github.com/nuxt-modules/icon?tab=readme-ov-file
- Custom nuxt icon by local svg: https://github.com/adinvadim/nuxt-local-iconify
## Prefix
