import { beforeEach, describe, expect, it } from 'vitest'
import type { VueWrapper } from '@vue/test-utils'
import { mount } from '@vue/test-utils'
import TestBase from '@/components/base/TestBase.vue'

describe('testBase.nuxt.spec.ts', () => {
  const wrapper: VueWrapper<InstanceType<typeof TestBase>> = mount(TestBase)

  beforeEach(async () => {
    await wrapper.setProps({
      testingProps: ['a', 'b'],
    })
  })

  // testing props and define component
  it('should be receive ["a", "b"]', () => {
    expect(wrapper.vm.testingProps).toEqual(['a', 'b'])
  })

  it('components be defined', () => {
    expect(wrapper.vm).toBeDefined()
  })

  it('should be match snapshot', () => {
    expect(wrapper.vm).toMatchSnapshot()
  })
})
