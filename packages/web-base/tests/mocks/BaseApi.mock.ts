import { vi } from 'vitest'

export default vi.fn(() => {
  return {
    getTestingList: vi.fn().mockResolvedValue({}),
  }
})
