import { describe, expect, it } from 'vitest'
import { sum } from '~/utils/testing'

describe('testing.nuxt.spec.ts', () => {
  it('test 1 + 2 should be 3', () => {
    expect(sum(1, 2)).toBe(3)
  })
})
