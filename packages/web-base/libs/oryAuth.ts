import type { FrontendApi, UpdateLoginFlowBody } from '@ory/client'

abstract class AuthService {
  protected abstract frontendApi: FrontendApi

  abstract login(email: string, password: string): Promise<any>
  abstract logout(): Promise<any>
  abstract getSession(): Promise<any>
}

class OryAuth extends AuthService {
  protected frontendApi: FrontendApi

  constructor(frontendApi: FrontendApi) {
    super()
    this.frontendApi = frontendApi
  }

  async login(email: string, password: string): Promise<any> {
    const { data: flow } = await this.frontendApi.createBrowserLoginFlow()
    const updateLoginFlowBody: UpdateLoginFlowBody = {
      csrf_token: flow.ui.nodes[0]?.attributes.value,
      identifier: email,
      password,
      method: 'password',
    } as UpdateLoginFlowBody
    return this.frontendApi.updateLoginFlow({ flow: flow.id, updateLoginFlowBody })
  }

  async logout(): Promise<any> {
    const { data: flow } = await this.frontendApi.createBrowserLogoutFlow()
    return this.frontendApi.updateLogoutFlow({ token: flow.logout_token })
  }

  async getSession(): Promise<any> {
    return this.frontendApi.toSession()
  }
}

export default OryAuth
