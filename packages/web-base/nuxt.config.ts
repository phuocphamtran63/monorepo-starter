// https://nuxt.com/docs/api/configuration/nuxt-config
import { createResolver } from '@nuxt/kit'

const { resolve } = createResolver(import.meta.url)

export default defineNuxtConfig({
  devtools: { enabled: true },
  alias: { '~base': resolve('./') },
  components: [
    { path: '~base/components', prefix: 'Ec' },
  ],
  css: [
    '~base/assets/scss/main.scss',
  ],
  modules: [
    'nuxt-headlessui',
    '@nuxtjs/i18n',
    '@pinia/nuxt',
    '@hebilicious/vue-query-nuxt',
    '@nuxt/test-utils/module',
    '@vee-validate/nuxt',
    'nuxt-zod-i18n',
    '@nuxt/ui',
    'nuxt-local-iconify',
  ],
  imports: {
    dirs: ['stores'],
  },
  i18n: {
    strategy: 'prefix_except_default',
    defaultLocale: 'en',
    langDir: './locales',
    locales: [
      { code: 'en', file: 'en.json' },
      { code: 'vi', file: 'vi.json' },
      { code: 'ko', file: 'ko.json' },
    ],
  },
  zodI18n: {
    useModuleLocale: false,
  },
  headlessui: {
    prefix: 'Headless',
  },
  colorMode: {
    classSuffix: '',
    preference: 'system',
    fallback: 'light',
  },
  localIconify: {
    iconPath: './assets/icons',
  },
  runtimeConfig: {
    public: {
      apiBase: process.env.NUXT_PUBLIC_API_BASE,
      kratosPublicEndpoint: process.env.PUB_KRATOS_PUBLIC_ENDPOINT,
    },
  },
  ssr: false,
})
