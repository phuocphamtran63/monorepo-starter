---
Component: 'Modal'
Description: 'Document base modal'
Updated: 'May 03, 2024'
---

# Modal

## Props

| Props                             | Type             | Default |
|:----------------------------------|:-----------------|:--------|
| `v-model`                         | boolean          | false   |
| `clickOutsideToClose`             | boolean          | true    |
| `backdropOpacity`                 | 'sm', 'lg', 'xl' | 'sm'    |
| `type`                            | 'free', 'layout' | 'free'  |
---

## Usage

`component.vue`

```vue
<script setup lang="ts">
const isShowFree = ref(false)
const isShowLayout = ref(false)
const isShowCustomStyle = ref(false)
</script>

<template>
  <div>
    // Free styling modal
    <BaseModal v-model="isShowFree">
      content
    </BaseModal>

    // Layout modal
    <BaseModal
      v-model="isShowLayout"
      type="layout"
    >
      <template #title />
      <template #default />
      <template #action />
    </BaseModal>
  </div>
</template>
```
---

Custom background and style for modal
```vue
<template>
  <div>
    <BaseModal
      v-model="isShowCustomStyle"
      class="bg-gray-900 custom-class"
    >
      <div class="title" />
      <div class="content" />
      <div class="action" />
    </BaseModal>
  </div>
</template>

<stlye lang="scss">
  .custom-class {
    padding: 1rem
    .title {
      // your style
    }
    ...
  }
</stlye>
```
