---
Title: 'Alert'
Description: 'Document base alert'
Updated: 'May 03, 2024'
---

# Alert

`composables/useAlert.ts`

```ts
import type { SweetAlertOptions } from 'sweetalert2'
// https://github.com/sweetalert2/sweetalert2/blob/main/sweetalert2.d.ts

type IAlert = SweetAlertOptions & {
  title: string
  text?: string
  confirmButtonText?: string
  actionConfirm?: () => void
  actionCancel?: () => void
  customClass?: 'success' | 'error' | 'warning' | 'confirm' | string
}
```

## Functions

| Functions   | Description                                                                                          |
|:------------|:-----------------------------------------------------------------------------------------------------|
| `success()` | Default style alert success with your `text` & `title`                                               |
| `error()`   | Default style alert error with your `text` & `title`                                                 |
| `warning()` | Default style alert warning with your `text` & `title`                                               |
| `confirm()` | Default style alert confirm with your `text` & `title` and set `action` when click confirm or cancel |
| `show()`    | Using this function to custom alert!                                                                 |
## Parameters
| Parameter           | Type                                       | Default                |
|:--------------------|:-------------------------------------------|:-----------------------|
| `title`             | string                                     | 'Alert'                |
| `text`              | string                                     | 'Content message'      |
| `confirmButtonText` | string                                     | 'OK'                   |
| `actionConfirm`     | void                                       | undefined              |
| `actionCancel`      | void                                       | undefined              |
| `customClass`       | success, error, warning, confirm or string | 'yc-alert'             |
| `imageUrl`          | string                                     | '/icons/success.svg'   |

More parameters in [here](https://github.com/sweetalert2/sweetalert2/blob/main/sweetalert2.d.ts)!

---
## Usage
### In pages | components
`component.vue`
```vue
<script setup lang="ts">
// get success() from useAlert composable
const { success } = useAlert()

function showSuccess() {
  // call success with options
  success({
    text: 'This is success alert!',
  })
}
</script>

<template>
  <UButton @click="showSuccess">
    Success
  </UButton>
</template>
```

### Without nuxt
`api/index.vue`
```ts
import Swal from 'sweetalert2'
import { SweetAlertOptions } from '~base/plugins/sweetalert2.client' // default config swal

export function customInstance<T>(config: AxiosRequestConfig): Promise<T> {
  const source = Axios.CancelToken.source()
  const promise = AXIOS_INSTANCE({ ...config, cancelToken: source.token }).then(
    ({ data }) => {
      Swal.fire({
        ...SweetAlertOptions,
        title: 'Api success alert',
        customClass: 'Call success',
      })

      return data
    },
  )

  return promise
}
```
