import { Configuration, FrontendApi } from '@ory/client'
import OryAuth from '~/libs/oryAuth'

export default defineNuxtPlugin(() => {
  const { $config } = useNuxtApp()
  const frontendApi: FrontendApi = new FrontendApi(new Configuration({
    basePath: $config.public.kratosPublicEndpoint as string,
    baseOptions: {
      withCredentials: true,
    },
  }))
  const oryAuth: OryAuth = new OryAuth((frontendApi))
  return {
    provide: {
      oryAuth,
    },
  }
})
