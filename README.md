# monorepo-starter
This project use Nuxt Layers to build a complete monorepo architecture use Yarn.
- To read more about report FE Dev in Notion, check [its documentation](https://www.notion.so/Echomakr-d9cf821ceefe49039f7d6d046dd83f8e).
## Development
Run Workspace
```bash
yarn workspace:dev
```
Run layer
```bash
yarn base:dev
```
Run web-user
```bash
yarn user:dev
```
Run web-admin
```bash
yarn user:admin
```
Run generate API
```bash
yarn user:api
```
## Dependencies
- Add --dev flag when installing devDependencies if only needed for development and testing.
- Add # why you need this package
```shell
yarn add typescript eslint @antfu/eslint-config #config eslint
```
## Prefix
> "Headless UI": which is currently set to start 'Headless'

> "Using Component Prefix": in layer web-base prefix setting to uniformly prefix start "Ec" (It is the abbreviation for Echomark).


## Rule
- To lint your conventional commits, check [its documentation](https://github.com/conventional-changelog/commitlint/tree/master/@commitlint/config-conventional).
## Documentation
- To learn more about Workspaces with Yarn, check [its documentation](https://classic.yarnpkg.com/lang/en/docs/workspaces/).
- To learn more about Monorepo architecture, check [its documentation](https://serko.dev/post/nuxt-3-monorepo#architectural-design-goals).
- To learn more about Extended ESLint rules with Antfu, check [its documentation](https://github.com/antfu/eslint-config).
