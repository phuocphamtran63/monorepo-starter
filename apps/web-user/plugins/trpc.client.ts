import { createTRPCNuxtClient, httpBatchLink } from 'trpc-nuxt/client'
import type { TRPCRouter } from '../../hono-tRPC/src/trpc/routers'

export default defineNuxtPlugin(() => {
  const client = createTRPCNuxtClient<TRPCRouter>({
    links: [
      httpBatchLink({
        url: 'http://localhost:8787/trpc',
        fetch(url, options) {
          return fetch(url, {
            ...options,
            credentials: 'include',
          })
        },
      }),
    ],
  })

  return {
    provide: {
      client,
    },
  }
})
