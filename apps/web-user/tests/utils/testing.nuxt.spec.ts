import { afterEach, describe, expect, it, vi } from 'vitest'

describe('math.add', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('should add two numbers correctly', () => {
    expect(math.add(1, 2)).toBe(3)
  })

  it('should call add function with correct arguments', () => {
    const spy = vi.spyOn(math, 'add')
    math.add(1, 2)
    expect(spy).toHaveBeenCalledWith(1, 2)
    expect(spy).toHaveBeenCalledTimes(1)
  })

  it('should not affect the original implementation', () => {
    vi.spyOn(math, 'add')
    expect(math.add(1, 2)).toBe(3)
  })
})
