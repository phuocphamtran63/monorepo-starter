import { beforeEach, describe, expect, it } from 'vitest'
import type { VueWrapper } from '@vue/test-utils'
import { shallowMount } from '@vue/test-utils'
import { EcBaseTestBase } from '#components'

describe('testBase.nuxt.spec.ts', () => {
  let wrapper: VueWrapper<InstanceType<typeof EcBaseTestBase>>

  beforeEach(async () => {
    wrapper = shallowMount(EcBaseTestBase)
  })

  it('components be defined', () => {
    expect(wrapper.vm).toBeDefined()
  })

  it('should be match snapshot', () => {
    expect(wrapper.vm).toMatchSnapshot()
  })
})
