// https://nuxt.com/docs/api/configuration/nuxt-config
import { createResolver } from '@nuxt/kit'

const { resolve } = createResolver(import.meta.url)
export default defineNuxtConfig({
  devtools: { enabled: true },
  extends: ['../../packages/web-base'],
  alias: { '~user': resolve('./') },
  components: [
    { path: '~user/components' },
  ],
  ssr: false,
  build: {
    transpile: ['trpc-nuxt'],
  },
  localIconify: {
    iconPath: '../../packages/web-base/assets/icons',
  },
  devServer: {
    host: 'localhost.nip.io',
  },
})
