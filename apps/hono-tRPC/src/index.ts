import { serve } from '@hono/node-server'
import { Hono } from 'hono'
import { trpcServer } from '@hono/trpc-server' // Deno 'npm:@hono/trpc-server'
import { cors } from 'hono/cors'
import { trpcRouter } from './trpc/routers'

const app = new Hono()

app.use(
  cors({
    origin: 'http://localhost:3000',
    credentials: true,
  }),
)

app.get('/', (c) => {
  return c.text('Hello Node.js!!')
})
app.use(
  '/trpc/*',
  trpcServer({
    router: trpcRouter,
    createContext: (opts, c) => ({
      honoContext: c,
    }),
  }),
)

serve({
  fetch: app.fetch,
  port: 8787,
})
