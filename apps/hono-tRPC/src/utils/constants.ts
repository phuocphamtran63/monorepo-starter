export const SITES = {
  COMICO: {
    URL: 'https://translate.comico.io',
    COOKIE_NAME: 'COMICO_TRANS_ADMIN_TOKEN',
  },
  KILEDDEL: {
    URL: 'https://tms.kiledel.com',
    COOKIE_NAME: 'PHPSESSID',
  },
  KENAZ: {
    URL: 'https://novel.kenazcp.com',
    API_URL: 'https://api.kenazcp.com',
    COOKIE_NAME: '',
  },
}
