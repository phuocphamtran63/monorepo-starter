import type { Context } from 'hono'
import { setCookie } from 'hono/cookie'

export function setCookieByResponse(honoContext: Context, cookie: string) {
  const firstPair = cookie.split('; ')[0]
  const [COOKIE_NAME, token] = firstPair.split('=')

  const attributes = cookie.split('; ').reduce((acc: { [key: string]: string }, curr) => {
    const [key, value] = curr.split('=')
    acc[key] = value
    return acc
  }, {})

  setCookie(honoContext, COOKIE_NAME, token, {
    httpOnly: true,
    sameSite: 'Strict',
    maxAge: attributes['Max-Age'] ? Number.parseInt(attributes['Max-Age']) : undefined,
    expires: attributes.Expires ? new Date(attributes.Expires) : undefined,
  })
}
