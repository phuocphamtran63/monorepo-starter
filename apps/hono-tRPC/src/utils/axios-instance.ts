import https from 'node:https'
import axios from 'axios'

// Create a new HTTP agent
const agent = new https.Agent({
  rejectUnauthorized: false,
  keepAlive: true,
})

// Create an axios instance with the custom HTTP agent
const axiosInstance = axios.create({
  httpsAgent: agent,
})

export default axiosInstance
