import { initTRPC } from '@trpc/server'
import type { Context } from 'hono'

interface HonoContext {
  honoContext: Context
}

const t = initTRPC.context<HonoContext>().create()

export const publicProcedure = t.procedure
export const router = t.router
