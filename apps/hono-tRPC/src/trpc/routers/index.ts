import { router } from '../index'
import { bringRouter } from './bring'

export const trpcRouter = router({
  bring: bringRouter,
})

export type TRPCRouter = typeof trpcRouter
