import { router } from '../../../index'
import authRouter from './auth'
import episodeRouter from './episode'

export const comicoRouter = router({
  auth: authRouter,
  episode: episodeRouter,
})
