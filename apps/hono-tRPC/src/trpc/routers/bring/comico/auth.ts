import { z } from 'zod'
import { publicProcedure, router } from '../../../index'
import { SITES } from '../../../../utils/constants'
import axiosInstance from '../../../../utils/axios-instance'
import { setCookieByResponse } from '../../../../utils/cookie'

export default router({
  login: publicProcedure.input(z.object({ username: z.string(), password: z.string() }))
    .query(async ({ input, ctx }) => {
      const URL = `${SITES.COMICO.URL}/api/login`

      const formData = new FormData()
      formData.append('email', input.username)
      formData.append('password', input.password)

      const response = await axiosInstance.post(URL, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })

      if (response.headers['set-cookie']) {
        const cookie = response.headers['set-cookie'][0]
        if (cookie)
          setCookieByResponse(ctx.honoContext, cookie)
      }

      return `Hello ${input.username}!!!!!!!!!`
    }),
})
