import { z } from 'zod'
import { getCookie } from 'hono/cookie'
import { publicProcedure, router } from '../../../index'

export default router({
  getById: publicProcedure.input(z.number())
    .query(({ input, ctx }) => {
      const cookie = getCookie(ctx.honoContext)
      if (cookie)
        return `Woww cookie`

      return `Hello ${input}!!!!!!!!!`
    }),
})
